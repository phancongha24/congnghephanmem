package com.sago.SagoStay.Specification;

import com.sago.SagoStay.Model.Meta.PostDetail_;
import com.sago.SagoStay.Model.Post;
import com.sago.SagoStay.Model.PostDetail;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.sago.SagoStay.Specification.SearchOperation.EQUALITY;
import static com.sago.SagoStay.Specification.SearchOperation.NEGATION;
import static com.sago.SagoStay.Specification.SearchOperation.GREATER_THAN;
import static com.sago.SagoStay.Specification.SearchOperation.LESS_THAN;
import static com.sago.SagoStay.Specification.SearchOperation.LIKE;
import static com.sago.SagoStay.Specification.SearchOperation.STARTS_WITH;
import static com.sago.SagoStay.Specification.SearchOperation.ENDS_WITH;
import static com.sago.SagoStay.Specification.SearchOperation.CONTAINS;
@AllArgsConstructor
public class PostDetailSpecification implements Specification<PostDetail>
{
    private SearchCriteria criteria;
    @Override
    public Predicate toPredicate(
            Root<PostDetail> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        switch (criteria.getOperation()) {
            case EQUALITY:
                return builder.equal(root.get(criteria.getKey()), criteria.getValue());
            case NEGATION:
                return builder.notEqual(root.get(criteria.getKey()), criteria.getValue());
            case GREATER_THAN:
                return builder.greaterThanOrEqualTo(root.<String> get(
                        criteria.getKey()), criteria.getValue().toString());
            case LESS_THAN:
                return builder.lessThanOrEqualTo(root.<String> get(
                        criteria.getKey()), criteria.getValue().toString());
            case LIKE:
                return builder.like(root.<String> get(
                        criteria.getKey()), "%"+criteria.getValue().toString()+"%");
            case STARTS_WITH:
                return builder.like(root.<String> get(criteria.getKey()), criteria.getValue() + "%");
            case ENDS_WITH:
                return builder.like(root.<String> get(criteria.getKey()), "%" + criteria.getValue());
            case CONTAINS:
                return builder.like(root.<String> get(
                        criteria.getKey()), "%" + criteria.getValue() + ",%");
            default:
                return null;
        }
    }
    public static Specification<PostDetail> hasFacility(String chuoiTienIch) {
        return (root, query, cb) -> cb.equal(root.get(PostDetail_.CHUOITIENICH), chuoiTienIch);
    }
    public static Specification<PostDetail> hasPriceBetween(Long min, Long max) {
        return (root, query, cb) -> cb.between(root.get(PostDetail_.GIA.replace(".","")), min, max);
    }
    public static Specification<PostDetail> hasDistrict(String districts) {
        return (root, query, cb) -> cb.like(root.get(PostDetail_.DIACHI), "%" + districts + ",%");
    }
    public static Specification<PostDetail> hasAreaBetween(Long min, Long max) {
        return (root, query, cb) -> cb.between(root.get(PostDetail_.DIENTICH), min, max);
    }
}
