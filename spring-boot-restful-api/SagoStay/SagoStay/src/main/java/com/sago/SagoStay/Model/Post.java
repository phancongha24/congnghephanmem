package com.sago.SagoStay.Model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Builder;
import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name="post")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Post extends DateEntity {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(name = "tieude")
    private String tieuDe;


    @Column(name = "status")
    private String status;

    @NotBlank
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    @Column(name="luotxem",columnDefinition = "bigint(20) default 0")
    private Long luotXem = 0L;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY,mappedBy = "post" )
    private List<History> listHistory = new ArrayList<>();

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY,mappedBy = "postId")
    private List<WishList> wishList = new ArrayList<>();

    @OneToOne(mappedBy = "post")
    private PostDetail postDetail;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "post")
    private List<Image> imageList = new ArrayList<>();
    @JsonIgnore
    public User getUser(){
        return  this.user;
    }


}
