package com.sago.SagoStay.Payload;

import com.sun.istack.Nullable;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import javax.validation.constraints.NotBlank;
@Data
@JsonPropertyOrder({
        "reportId",
        "reason",
        "status"
})
public class HandleReportRequest {
    @NotBlank
    private Long reportId;
    @Nullable
    private String reason;
    @Nullable
    private String status;
}