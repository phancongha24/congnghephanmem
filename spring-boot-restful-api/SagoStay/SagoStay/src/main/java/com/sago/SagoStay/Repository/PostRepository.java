package com.sago.SagoStay.Repository;

import com.sago.SagoStay.Model.Post;
import com.sago.SagoStay.Model.PostDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional(rollbackOn = Exception.class)
public interface PostRepository extends JpaRepository<Post,Long>, PagingAndSortingRepository<Post, Long>
{

    List<Post> findAllByUserId(Long userID);
    boolean existsById(Long postID);
}
