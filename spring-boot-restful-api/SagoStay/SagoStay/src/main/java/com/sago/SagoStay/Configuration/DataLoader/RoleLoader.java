package com.sago.SagoStay.Configuration.DataLoader;

import com.sago.SagoStay.Model.Role;
import com.sago.SagoStay.Service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public class RoleLoader implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private RoleService roleService;
    private boolean dataLoaded = false;
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        if(dataLoaded) return;
        //createRoleIfNotFound("Quản lý nhân viên");
        createRoleIfNotFound("Quản lý bài đăng");
        createRoleIfNotFound("Chăm sóc khách hàng");
        createRoleIfNotFound("Quản lý tài khoản");
        dataLoaded = true;
    }
    @Transactional
    void createRoleIfNotFound(String RoleName){
        if(roleService.existsByTenQuyen(RoleName)) return;
        roleService.save(new Role(RoleName));
    }
}
