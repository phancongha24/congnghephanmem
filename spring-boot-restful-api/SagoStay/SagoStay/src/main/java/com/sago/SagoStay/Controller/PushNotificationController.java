package com.sago.SagoStay.Controller;

import com.sago.SagoStay.Model.Notification;
import com.sago.SagoStay.Model.User;
import com.sago.SagoStay.Payload.BaseResponseEntity;
import com.sago.SagoStay.Payload.NotifyAllRequest;
import com.sago.SagoStay.Payload.PnsRequest;
import com.sago.SagoStay.Repository.NotificationRepository;
import com.sago.SagoStay.Service.ExpoNotificationService;
import com.sago.SagoStay.Service.FCMService;
import com.sago.SagoStay.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/notify")
public class PushNotificationController {
    @Autowired
    private FCMService fcmService;
    @Autowired
    private ExpoNotificationService expoNotificationService;
    @Autowired
    private NotificationRepository notificationRepository;
    @PostMapping("/send")
    public String sendNotification(@RequestBody PnsRequest pnsRequest){
                return fcmService.pushNotification(pnsRequest);
        }
    @GetMapping
    public void sendNotification(){
        expoNotificationService.sendMessage("Test",notificationRepository.findById(4L).get());
    }
}
