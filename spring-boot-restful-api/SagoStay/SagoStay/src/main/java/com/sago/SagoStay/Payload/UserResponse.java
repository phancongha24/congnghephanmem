package com.sago.SagoStay.Payload;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@JsonPropertyOrder({
        "id",
        "hoTen",
        "email",
})
@Data
@AllArgsConstructor
public class UserResponse{
    @JsonProperty
    private Long id;
    @JsonProperty
    private String hoTen;
    @JsonProperty
    private String email;
}