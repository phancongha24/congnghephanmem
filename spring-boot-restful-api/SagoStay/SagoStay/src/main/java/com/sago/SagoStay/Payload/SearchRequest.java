package com.sago.SagoStay.Payload;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.Nullable;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@JsonPropertyOrder({
        "minPrice",
        "maxPrice",
        "minArea",
        "maxArea",
        "facility",
        "district"
})
public class SearchRequest {
    @Nullable
    private Long minPrice;
    @Nullable
    private Long maxPrice;
    @Nullable
    private Long minArea;
    @Nullable
    private Long maxArea;
    @Nullable
    private String facility;
    @Nullable
    @JsonProperty
    private List<String> districts;
}