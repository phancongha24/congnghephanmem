package com.sago.SagoStay.Service;

import com.sago.SagoStay.Payload.PnsRequest;

public interface FCMService {
    String pushNotification(PnsRequest PnsRequest);
}
