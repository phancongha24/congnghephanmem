package com.sago.SagoStay.Controller;

import com.sago.SagoStay.Model.User;
import com.sago.SagoStay.Model.VerificationToken;
import com.sago.SagoStay.Payload.*;
import com.sago.SagoStay.Security.JWTokenProvider;
import com.sago.SagoStay.Service.EmailService;
import com.sago.SagoStay.Service.StaffService;
import com.sago.SagoStay.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Calendar;
import java.util.UUID;

@RequestMapping("api/auth")
@RestController
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    private UserService userService;
    @Autowired
    private StaffService staffService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private EmailService emailService;
    @Autowired
    private JWTokenProvider jwTokenProvider;

    @PostMapping("/user/signup")
    public ResponseEntity<BaseResponseEntity> signUp(@Valid @RequestBody UserSignUpRequest signUpRequest) {

        if (userService.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity<>(new BaseResponseEntity(-1, "Email đã tồn tại"), HttpStatus.OK);
        }
        try {
            User user = new User(signUpRequest.getHoTen(), signUpRequest.getSoDienThoai(), signUpRequest.getEmail(), passwordEncoder.encode(signUpRequest.getPassword()),false,false);
            String Token = UUID.randomUUID().toString();
            userService.save(user);
            userService.createToken(user,Token);
            new Thread(()->{
                emailService.sendingVerifiedEmail(user.getEmail(),Token);
            }).start();
            return new ResponseEntity<>(new BaseResponseEntity(1, "Đăng ký thành công"), HttpStatus.OK);

        } catch (Exception ex) {
            return ResponseEntity.badRequest().body(null);
        }
    }
    @PostMapping("/user/signin")
    public ResponseEntity<BaseResponseEntity> signIn(@Valid @RequestBody UserSignInRequest userSignInRequest){

      try{
          if(!userService.existsByEmail(userSignInRequest.getEmail()))
                return new ResponseEntity<>(new BaseResponseEntity(0,"Email không tồn tại"),HttpStatus.OK);
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            userSignInRequest.getEmail(),
                            userSignInRequest.getPassword()
                    )
            );
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String Token = jwTokenProvider.createUserDetailToken(authentication);
            return new ResponseEntity<>(new JWTResponse(1, "Đăng nhập thành công", Token), HttpStatus.OK);
    }catch(Exception ex){
        return new ResponseEntity<>(new BaseResponseEntity(-1, "Đăng nhập thất bại"), HttpStatus.OK);
    }
    }
    @PostMapping("/staff/signin")
    public ResponseEntity<BaseResponseEntity> signIn(@Valid @RequestBody StaffSignInRequest staffSignInRequest) {
        try {
            if (!staffService.existsByUsername(staffSignInRequest.getUsername()))
                return new ResponseEntity<>(new BaseResponseEntity(0, "Username không tồn tại"), HttpStatus.OK);

            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                            staffSignInRequest.getUsername(), staffSignInRequest.getPassword()
                    )
            );
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String Token = jwTokenProvider.createStaffDetailToken(authentication);
            return new ResponseEntity<>(new JWTResponse(1, "Đăng nhập thành công", Token), HttpStatus.OK);
        }catch(Exception ex){
            return new ResponseEntity<>(new BaseResponseEntity(-1, "Đăng nhập thất bại"), HttpStatus.OK);
        }
        }
    @GetMapping("/sendemail")
    public ResponseEntity<BaseResponseEntity> sendEmail(@RequestParam("email") String Email){
        User user = userService.findByEmail(Email);
        String Token = UUID.randomUUID().toString();
        userService.createToken(user,Token);
        new Thread(()->{
            emailService.sendingVerifiedEmail(user.getEmail(),Token);
        }).start();
        return new ResponseEntity<>(new BaseResponseEntity(1,"Gửi mail thành công"),HttpStatus.OK);
    }
    @GetMapping("/verify")
    public ResponseEntity<BaseResponseEntity> verifyToken(@RequestParam(value="token")String Token){
        try{
            VerificationToken verificationToken = userService.getToken(Token);
            if(verificationToken == null)
                return new ResponseEntity<>(new BaseResponseEntity(-1,"Token không hợp lệ"),HttpStatus.OK);
            if(verificationToken.getExpiredDate().getTime() - Calendar.getInstance().getTime().getTime() <=0)
                return new ResponseEntity<>(new BaseResponseEntity(0,"Token đã hết hạn"), HttpStatus.OK);
            User user =  verificationToken.getUser();
            if(user.isVerifiedEmail())
                return new ResponseEntity<>(new BaseResponseEntity(0,"Tài khoản đã xác thực email"),HttpStatus.OK);
            user.setVerifiedEmail(true);
            userService.save(user);
            userService.deleteAllToken(user);
            return new ResponseEntity<>(new BaseResponseEntity(1,"Xác thực email thành công"),HttpStatus.OK);
        } catch (Exception ex){
            ex.printStackTrace();
            return ResponseEntity.badRequest().body(null);
        }
    }
}