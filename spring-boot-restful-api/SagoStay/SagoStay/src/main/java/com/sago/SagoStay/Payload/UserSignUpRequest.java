package com.sago.SagoStay.Payload;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class UserSignUpRequest {
    @NotBlank
    private String hoTen;

    @NotBlank
    private String email;

    @NotBlank
    private String soDienThoai;

    @NotBlank
    private String password;
}
