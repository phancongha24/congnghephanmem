package com.sago.SagoStay.Service.Impl;

import com.sago.SagoStay.Model.Role;
import com.sago.SagoStay.Repository.RoleRepository;
import com.sago.SagoStay.Service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleRepository roleRepository;
    @Override
    public String findTenQuyenById(Long id) {
        return roleRepository.findTenQuyenById(id);
    }

    public Role findRoleById(Long id){
        return roleRepository.findRoleById(id);
    }
    @Override
    public void save(Role role) {
        roleRepository.save(role)
;    }

    @Override
    public boolean existsByTenQuyen(String tenQuyen) {
        return roleRepository.existsByTenQuyen(tenQuyen);
    }
}
