package com.sago.SagoStay.Payload;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sago.SagoStay.Model.Post;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@JsonPropertyOrder({
        "hoTen",
        "email",
        "soDienThoai",
        "xacThucEmail",
        "danhSachBaiDang",
        "batThongBao"
})
@Data
@AllArgsConstructor
public class MyProfileResponse {
    @JsonProperty
    private String hoTen;
    @JsonProperty
    private String email;
    @JsonProperty
    private String soDienThoai;
    @JsonProperty
    private Boolean xacThucEmail;
    @JsonProperty
    private List<PostResponse> danhSachBaiDang;
    @JsonProperty
    private String soThongBao;
    @JsonProperty
    private Boolean batThongBao;
    public MyProfileResponse(String hoTen,String email,String soDienThoai,Boolean xacThucEmail,String soThongBao,Boolean batThongBao){
        this.hoTen=hoTen;
        this.email=email;
        this.soDienThoai=soDienThoai;
        this.xacThucEmail=xacThucEmail;
        this.soThongBao = soThongBao;
        this.batThongBao = batThongBao;
    }
}
