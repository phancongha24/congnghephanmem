package com.sago.SagoStay.Controller;

import com.sago.SagoStay.Model.Image;
import com.sago.SagoStay.Model.Post;
import com.sago.SagoStay.Model.User;
import com.sago.SagoStay.Payload.*;
import com.sago.SagoStay.Repository.FacilityRepository;
import com.sago.SagoStay.Repository.PostRepository;
import com.sago.SagoStay.Security.UserDetail;
import com.sago.SagoStay.Service.FileStorageService;
import com.sago.SagoStay.Service.PostService;
import com.sago.SagoStay.Service.StaffService;
import com.sago.SagoStay.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/posts")
public class PostController {
    @Autowired
    private PostService postService;
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private FacilityRepository facilityRepository;
    @Autowired
    private StaffService staffService;
    @Autowired
    private UserService userService;
    @Autowired
    private FileStorageService fileStorageService;
    private String baseUrl = "users/image/";
    @GetMapping
    public ResponseEntity<List<PostResponse>> getAllPosts()
    {
        return new ResponseEntity<>(postService.getAllApprovedPosts(),HttpStatus.OK);
    }
    @GetMapping("/staffs")
    @PreAuthorize("hasAuthority('1') or hasAuthority('3')")
    public ResponseEntity<List<PostResponse>> getAllPostsByStaffs()
    {
        return new ResponseEntity<>(postService.getAllPosts(),HttpStatus.OK);
    }
    @GetMapping("/pending")
    @PreAuthorize("hasAuthority('1') or hasAuthority('3')")
    public ResponseEntity<List<PostResponse>> getAllPendingPosts()
    {
        return new ResponseEntity<>(postService.getAllPendingPosts(),HttpStatus.OK);
    }
    @GetMapping("/rejected")
    @PreAuthorize("hasAuthority('1') or hasAuthority('3')")
    public ResponseEntity<List<PostResponse>> getAllRejectedPosts()
    {
        return new ResponseEntity<>(postService.getAllRejectedPosts(), HttpStatus.OK);
    }
    @PostMapping("/rejected")
    public ResponseEntity<List<PostResponse>> getAllRejectedPostsOfUser(@AuthenticationPrincipal UserDetail userDetail)
    {
        return new ResponseEntity<>(postService.getAllRejectedPostsOfUser(userDetail.getId()), HttpStatus.OK);
    }
    @PostMapping("/approved")
    public ResponseEntity<List<PostResponse>> getAllApprovedPostsOfUser(@AuthenticationPrincipal UserDetail userDetail)
    {
        return new ResponseEntity<>(postService.getAllApprovedPostsOfUser(userDetail.getId()), HttpStatus.OK);
    }
    @PostMapping("/pending")
    public ResponseEntity<List<PostResponse>> getAllPendingPostsOfUser(@AuthenticationPrincipal UserDetail userDetail)
    {
        return new ResponseEntity<>(postService.getAllPendingPostsOfUser(userDetail.getId()), HttpStatus.OK);
    }
    @GetMapping("/deleted")
    @PreAuthorize("hasAuthority('1') or hasAuthority('3')")
    public ResponseEntity<List<PostResponse>> getAllDeletedPosts()
    {
        return new ResponseEntity<>(postService.getAllDeletedPosts(), HttpStatus.OK);
    }
    @PutMapping
    public ResponseEntity updatePosts(@Valid @RequestParam(name="post") PostRequest postRequest,@Valid @RequestParam(name="images") MultipartFile[] Files,@AuthenticationPrincipal UserDetail userDetail)
    {
        User currentUser = userService.findByEmail(userDetail.getEmail());
        if(!currentUser.isVerifiedEmail())
            return new ResponseEntity(new BaseResponseEntity(0,"Email chưa xác thực"), HttpStatus.OK);
        Post post= postService.getPostById(postRequest.getPostID());
        if(post.getUser().getId().equals(userDetail.getId())){
            List<Image> imageList = new ArrayList<>(Files.length);
            for(MultipartFile file : Files) {
                Image image = new Image();
                String fileName = fileStorageService.storeFile(file);
                image.setUrl(baseUrl+fileName);
                image.setPost(post);
                imageList.add(image);
            }
            post.setTieuDe(postRequest.getTieuDe());
            postService.update(post,postRequest.getPostDetail(),imageList);
            return new ResponseEntity(new BaseResponseEntity(1,"Cập nhật thành công"), HttpStatus.OK);
        }
        return new ResponseEntity(new BaseResponseEntity(-1,"Cập nhật thất bại"), HttpStatus.OK);

    }
    @GetMapping("/statistic")
    public ResponseEntity statisticPosts()
    {
        return new ResponseEntity(postService.statistic(), HttpStatus.OK);
    }
    @PostMapping
    @PreAuthorize("hasAuthority('MEMBER')")
    public ResponseEntity<BaseResponseEntity> createPost(@Valid @RequestParam(name="post") PostRequest postRequest, @Valid @RequestParam(name="images") MultipartFile[] Files,@AuthenticationPrincipal UserDetail userDetail)
    {
        User currentUser = userService.findByEmail(userDetail.getEmail());
        if(!currentUser.isVerifiedEmail())
            return new ResponseEntity<BaseResponseEntity>(new BaseResponseEntity(0,"Email chưa xác thực"), HttpStatus.OK);
        Post post = new Post();
        List<Image> images = new ArrayList<>(Files.length);
        for(MultipartFile file : Files){
            Image image = new Image();
            String fileName = fileStorageService.storeFile(file);
            image.setPost(post);
            image.setUrl(baseUrl + fileName);
            images.add(image);
        }
        post.setTieuDe(postRequest.getTieuDe());
        System.out.println("asdasdasdsad"+postRequest.getPostDetail().getChuoiTienIch());
        post.setUser(currentUser);
        postService.create(post,postRequest.getPostDetail(),images);
        return new ResponseEntity<BaseResponseEntity>(new BaseResponseEntity(1,"Đăng bài thành công"), HttpStatus.OK);
    }
    @GetMapping("/{postID}")
    public ResponseEntity getPostResonseById(@PathVariable(name = "postID") Long postID,@AuthenticationPrincipal UserDetail userDetail)
    {
        if(!postService.existsById(postID))
            return new ResponseEntity(new BaseResponseEntity(-1,"Bài đăng không tồn tại"), HttpStatus.OK);
        PostResponse postResponse = postService.getPostResponse(postID);
        if(postResponse != null ){
            if(!postResponse.getTrangThai().equals("Approved")) {
                if (userDetail != null && postService.getPostById(postID).getUser().getId().equals(userDetail.getId())) {
                    postResponse.setIsAuthor(true);
                    return new ResponseEntity(postResponse, HttpStatus.OK);
                }
                return new ResponseEntity(new BaseResponseEntity(-1, "Bài đăng không tồn tại"), HttpStatus.OK);
            }
                postResponse.setIsAuthor(false);
                if(userDetail!=null)
                    postResponse.setIsAuthor(userDetail.getId().equals(postResponse.getPostDetail().getPost().getUser().getId()));
                return new ResponseEntity(postResponse,HttpStatus.OK);
        }
        return new ResponseEntity(new BaseResponseEntity(-1,"Bài đăng không tồn tại"), HttpStatus.OK);
    }
    @PostMapping("/demo")
    public ResponseEntity<BaseResponseEntity> demoCreatePost(@Valid @RequestBody PostRequest postRequest, @AuthenticationPrincipal UserDetail userDetail)
    {
        return new ResponseEntity<BaseResponseEntity>(new BaseResponseEntity(1,postRequest.toString()), HttpStatus.OK);
    }
    /**
     * delete()
     * change status of Post to "Deleted"
     * @param postId Long - stand for ID of post which user want to delete
     * @param userDetail UserDetail - contain User attributes get by Token in sending request
     * @return BaseResponseEntity
     */
    @PostMapping("/delete/{postId}")
    @PreAuthorize("hasAuthority('MEMBER')")
    public ResponseEntity<BaseResponseEntity> delete(@PathVariable(name = "postId") Long postId,@AuthenticationPrincipal UserDetail userDetail)
    {
        Post post = postService.getPostById(postId);
        if( post.getUser().getId() != userDetail.getId())
        {
            return new ResponseEntity(new BaseResponseEntity(0,"Bạn không sở hữu bài viết này"), HttpStatus.OK);
        }
        postService.delete(post);
        return new ResponseEntity(new BaseResponseEntity(1,"Xóa bài viết thành công"), HttpStatus.OK);
    }
    /**
     * getAdvancedSearch()
     * filtered Post base on criteria in searchRequest
     * @param searchRequest SearchRequest - contain User attributes get by Token in sending request
     * @return ResponseEntity
     */
    @PostMapping("/search")
    public ResponseEntity getAdvancedSearch(@Valid @RequestBody SearchRequest searchRequest)
    {
        return new ResponseEntity(postService.getAdvancedSearch(searchRequest),HttpStatus.OK);
    }
    @PostMapping("topview/{top}")
    @PreAuthorize("hasAuthority('1') or hasAuthority('3')")
    public ResponseEntity<List<TopViewResponse>> getTopView(@PathVariable(name = "top") int top)
    {
        return new ResponseEntity(postService.getTopViewResponse(top),HttpStatus.OK);
    }
}
