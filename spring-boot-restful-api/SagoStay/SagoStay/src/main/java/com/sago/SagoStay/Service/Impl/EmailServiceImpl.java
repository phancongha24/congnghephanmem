package com.sago.SagoStay.Service.Impl;

import com.sago.SagoStay.Service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;

@Service
public class EmailServiceImpl implements EmailService {
    @Autowired
    public JavaMailSender emailSender;
    @Override
    public void sendingVerifiedEmail(String To, String Token) {
        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper messageHelper = new MimeMessageHelper(message, "utf-8");
            message.setContent("http://192.168.38.2:8187/api/auth/verify?token="+Token,"text/html");
            messageHelper.setTo(To);
            messageHelper.setSubject("SagoStay -  Xác thực email");
            this.emailSender.send(message);
        } catch(Exception exception){

        }
    }

    @Override
    public void recoveryPasswordEmail(String To, String context) {
        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper messageHelper = new MimeMessageHelper(message, "utf-8");
            message.setContent("Recovery Password: "+context,"text/plain");
            messageHelper.setTo(To);
            messageHelper.setSubject("SagoStay -  Khôi phục tài khoản");
            this.emailSender.send(message);
        } catch(Exception exception){
            System.out.println("Error when sending email");
        }
    }
}
