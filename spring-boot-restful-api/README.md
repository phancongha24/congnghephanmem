## Explore Rest APIs

The app defines following CRUD APIs.

### Auth

| Method | Url | Decription | Sample Valid Request Body | 
| ------ | --- | ---------- | --------------------------- |
| POST   | /api/auth/user/signup | User Sign up | [JSON](#apiauthusersignup) |
| POST   | /api/auth/user/signin | User Sign in | [JSON](#apiauthusersignin) |
| POST   | /api/auth/staff/signin | Staff Sign in | [JSON](#apiauthstaffsignin) |
| GET   | /api/auth/verify | User verify account | [JSON](#apiauthverify) |

### Users

| Method | Url | Description | Sample Valid Request Body |
| ------ | --- | ----------- | ------------------------- |
| GET    | /api/users | Get all users |  |
| POST    | /api/users/profile | Get user profile | |
| GET    | /api/users/profile/{id} | Get user profile by id | |
| GET    | /api/users/recoverypassword | Recovery password | [JSON](#apiusersrecoverypassword)|
| POST    | /api/users/changepassword | Change user password | [JSON](#apiuserschangepassword) |
| POST    | /api/users/notification | Get all notifications of Logged User |  |
| GET    | /api/users/banned | Get all Banned User |  |
| GET    | /api/users/unbanned | Get all Unbanned User |  |


### Posts

| Method | Url | Description | Sample Valid Request Body |
| ------ | --- | ----------- | ------------------------- |
| GET    | /api/posts | Get all Approved posts (Except [Pending,Rejected] , deleted posts) ||
| POST    | /api/posts | create new Post (User only) [basic password = sago] | [Multipart Formdata](#apiposts) |
| GET   | /api/staffs | Get all post (Staff only) |  |
| PUT    | /api/posts | Update post (Belongs to Logged in User) | [Multipart Formdata](#apiposts) |
| GET    | /api/posts/pending | Get all pending posts (Staff only) ||
| GET    | /api/posts/rejected | Get all rejected posts (Staff only) ||
| POST    | /api/posts/approved | Get all approved posts (User only) ||
| POST    | /api/posts/rejected | Get all rejected posts (User only) ||
| POST    | /api/posts/pending | Get all pending posts (User only) ||
| GET    | /api/posts/{postID} | Get [approved] post detail by post ID (Only show [pending,rejected] posts belongs to logged in User) |  |
| GET | /api/posts/statistic | Statistic number of Posts as this format [Approved,Pending,Rejected,Deleted] | |
| DELETE | /api/posts/delete/{postId} | Delete post (Belongs to Logged in User) | |
| GET | /api/post/search | Filtered Post base on criteria in searchRequest |[JSON](#apipostssearch) |
| POST | /api/post/topview/{top} | Get Top (top) posts sort by descending view  | |

### Staffs

| Method | Url | Description | Sample Valid Request Body |
| ------ | --- | ----------- | ------------------------- |
| GET    | /api/staffs | Get all Staffs (Staff only) ||
| POST    | /api/staffs | create new Staff (Staff only) | [JSON](#apistaffs) |
| POST    | /api/staffs/update/{staffID} | Update Staff (Staff only) | [JSON](#apistaffsupdatestaffid) |
| POST    | /api/staffs/approving | Approving Post (Staff only) | [JSON](#apistaffsapproving)  |
| POST | /api/staffs/handling | Handling the report from User (Staff only)  |[JSON](#apistaffshandling) |
| POST | /api/staffs/ban/{userID} | Ban user (Staff only) | |
| POST | /api/staffs/unban/{userID} | Unban user (Staff only) | |
| POST | /api/staffs/notifyall | create Notification and notify to All User |[JSON](#apistaffsnotifyall) |


### Reports

| Method | Url | Description | Sample Valid Request Body |
| ------ | --- | ----------- | ------------------------- |
| POST | /api/reports | Report a Post (User only) | [JSON](#apireports)|
| GET | /api/reports | Get all Reports (Staff only) | |
| GET | /api/reports/pending | Get all Pending Reports (Staff only) | |

Test them using postman or any other rest client.

## Sample Valid JSON Request Bodys

#####  <a>/api/auth/user/signup</a>
**POST**
```json

{
	"hoTen":"Phan Cong Ha",
	"email":"sago@gmail.com",
	"soDienThoai":"0123445678",
	"password" : "sago"
}
```

##### <a>/api/auth/user/signin</a>
**POST**
```json
{
	"email": "phancongha24@gmail.com",
	"password": "password"
}
```
##### <a>/api/auth/staff/signin</a>
**POST**
```json
{
	"username": "system",
	"password": "sago"
}
```
##### <a>/api/auth/verify</a>
**GET**
```json
{
	"token":"RandomToken"
}
```

##### <a>/api/users/recoverypassword</a>
**GET**
```json
{
	"email": "sago@gmail.com"
}
```

##### <a>/api/users/changepassword</a>
**POST**
```json
{
	"oldPassword": "sago",
	"newPassword": "newSago"
}
```


##### <a>/api/posts</a>
**POST**
```json
----------------------------postman
Content-Disposition: form-data; name="post"

{"tieuDe":"CHDV Sân Bay TSN. Full Nội Thất Mức Giá Thuê Cực Rẻ",
"postDetail":{"gia":1500000,"datCoc":550000,"giaTienDien":1500000,"giaTienNuoc":100000,"giaTienWifi":100000,"dienTich":"40","sucChua":"5-10","trangThaiPhong":false,"diaChi":"2R Trường Sơn, Phường 2, Quận Tân Bình, Hồ Chí Minh","chuoiTienIch":"nvsr-bdx-wf","moTa":"Địa chỉ: 2R Trường Sơn, P2, Tân Bình\nMặt tiền, khu sân bay cao cấp.\nNgay lá phổi Xanh CV Hoàng Văn Thụ\n\n Free phí dịch vụ: Máy giặt + Sấy, rác, vệ sinh nhà hàng tuần, hút bụi tận phòng, để xe máy...\n\n Nội thất Full cao cấp: Sofa, giường tủ quần áo, tủ kệ bếp, bàn ăn, Smart tivi 40”, Máy lạnh, tủ lạnh, Bếp hồng ngoại cao cấp...\n\n Giá thuê: \n- CHDV 28m2, giá thuê 6.5 triệu -> Ưu đãi tháng đầu tiên 5.5 Triệu/ tháng ( Cửa sổ, logia)\n\n Quản lý chuyên nghiệp theo hệ thống, luôn có người bảo trì, vận hành thân thiện\n\n Xem phòng: 097.379.7286 Mr. Hùng\n)","soDienThoai":"0973797286"}
}
----------------------------postman
Content-Disposition: form-data; name="images"; filename="1602429708580_IMG_9751.png"

<1602429708580_IMG_9751.png>
----------------------------postman
Content-Disposition: form-data; name="images"; filename="2.png"

<2.png>
----------------------------postman
Content-Disposition: form-data; name="images"; filename="1.png"

<1.png>
----------------------------postman--

```

##### <a>/api/posts</a>
**PUT**
```json
 
 ----------------------------postman
Content-Disposition: form-data; name="post"

{"postID":1,"tieuDe":"CHDV Sân Bay TSN. Full Nội Thất Mức Giá Thuê Cực Rẻ",
"postDetail":{"gia":1500000,"datCoc":550000,"giaTienDien":1500000,"giaTienNuoc":100000,"giaTienWifi":100000,"dienTich":"40","sucChua":"5-10","trangThaiPhong":false,"diaChi":"2R Trường Sơn, Phường 2, Quận Tân Bình, Hồ Chí Minh","chuoiTienIch":"nvsr-bdx-wf","moTa":"Địa chỉ: 2R Trường Sơn, P2, Tân Bình\nMặt tiền, khu sân bay cao cấp.\nNgay lá phổi Xanh CV Hoàng Văn Thụ\n\n Free phí dịch vụ: Máy giặt + Sấy, rác, vệ sinh nhà hàng tuần, hút bụi tận phòng, để xe máy...\n\n Nội thất Full cao cấp: Sofa, giường tủ quần áo, tủ kệ bếp, bàn ăn, Smart tivi 40”, Máy lạnh, tủ lạnh, Bếp hồng ngoại cao cấp...\n\n Giá thuê: \n- CHDV 28m2, giá thuê 6.5 triệu -> Ưu đãi tháng đầu tiên 5.5 Triệu/ tháng ( Cửa sổ, logia)\n\n Quản lý chuyên nghiệp theo hệ thống, luôn có người bảo trì, vận hành thân thiện\n\n Xem phòng: 097.379.7286 Mr. Hùng\n)","soDienThoai":"0973797286"}
}
----------------------------postman
Content-Disposition: form-data; name="images"; filename="1602429708580_IMG_9751.png"

<1602429708580_IMG_9751.png>
----------------------------postman
Content-Disposition: form-data; name="images"; filename="2.png"

<2.png>
----------------------------postman
Content-Disposition: form-data; name="images"; filename="1.png"

<1.png>
----------------------------postman--

```
##### <a>/api/users/changepassword</a>
**POST**
```json
{
	"oldPassword": "sago",
	"newPassword": "newSago"
}
```

##### <a>/api/staffs</a>
**POST**
```json
{
        "hoTen":"Sago",
        "username":"sago",
        "namSinh":1999,
        "soDienThoai":"012345678",
        "luong":3999999,
        "roleType":1
}
```
##### <a>/api/staffs/update/staffID</a>
**POST**
```json
{
		
        "hoTen":"Sago",
        "username":"sago",
        "password":"newpassword",
        "namSinh":1999,
        "soDienThoai":"012345678",
        "luong":3999999,
        "roleType":1
}
```
##### <a>/api/staffs/approving</a>
**POST**
```json
{
		
        "postID":1,
        "trangThai":"Approved|Pending|Rejected",
        "liDo":""
}
```
##### <a>/api/staffs/handling</a>
**POST**
```json
{
		
       "reportId":1,
        "reason":"",
        "status":"Pending|Handled"
}
```
##### <a>/api/reports</a>
**POST**
```json
{
	"postId": 1,
	"reason": "Scaming"
}
```
##### <a>/api/posts/search</a>
**POST**
```json
{
        "minPrice": 0,
        "maxPrice": 1500000,
        "minArea": 41,
        "maxArea": 42,
        "facility": "tv-ad",
        "districts": ["Quận Bình Tân","Quận 7"]
}
```
##### <a>/api/staffs/notifyall</a>
**POST**
```json
{
        "tieuDe": "Bảo trì máy chủ"
        "noiDung": "Thông tin đền bù vui lòng truy cập: sagostay.denbu.com"
}
```
