import axios from "axios";
import React from "react";
import {Table,Button,Modal,Form,Col,InputGroup} from "react-bootstrap";
const thArrStaff = ["Mã nhân viên","Họ tên","Năm sinh","Số điện thoại","Mức lương","Cập nhật "];
const role = [
    {"id" : 1,"tenquyen":"Hệ thống"},
    {"id" : 2,"tenquyen":"Quản lý nhân viên"},
    {"id" : 3,"tenquyen":"Quản lý bài đăng"},
    {"id" : 4,"tenquyen":"Chăm sóc khách hàng"},
    {"id" : 5,"tenquyen":"Quản lý tài khoản"},
    ]
class TableStaff extends React.Component {
    constructor(){
        super();
        this.state ={
            data :[],
            dataUpdate: {},
            modalUpdate : false,
            txtName : null,
            txtBirthDay : null,
            txtPhoneNumber: null,
            txtUsername: null,
            txtWage: null,
            inputRole: null,
            validated : false
        }
        this.openModalUpdate = this.openModalUpdate.bind(this);
        this.closeModalUpdate = this.closeModalUpdate.bind(this);
    }
    openModalUpdate(item){
        this.setState({
            modalUpdate : true,
            dataUpdate : item.prop,
        })
        this.setState({
            txtName : item.prop.hoTen,
            txtBirthDay : item.prop.namSinh,
            txtPhoneNumber : item.prop.soDienThoai,
            txtUsername: item.prop.username,
            txtWage : item.prop.luong,
            inputRole : item.prop.roleType
        })
    }
    closeModalUpdate(){
        this.setState({
            modalUpdate : false
        })
        this.fetchDataStaff();
    }
    fetchDataStaff(){
        const token = localStorage.token;
        if(token){
        axios.get("http://localhost:8187/api/staffs",{
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            }).then(response =>{
                this.setState({
                    data : response.data
                })
                }
            )
        }
    }
    componentDidMount(){
        this.fetchDataStaff();
    }
    isInputChange = (event) =>{
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name] : value
        });
    }
    submitFormUpdate(event,id){
        const token = localStorage.token;
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
        }
        else{
            event.preventDefault();
            var content ={
                hoTen : this.state.txtName,
                username: this.state.txtUsername,
                namSinh : this.state.txtBirthDay,
                soDienThoai : this.state.txtPhoneNumber,
                luong: this.state.txtWage,
                roleType: this.state.inputRole
            };
            if(token){
            fetch(`http://localhost:8187/api/staffs/update/${id}`,{
                    method : "POST",
                    headers :{
                        "Content-Type" : "application/json",
                        'Authorization': `Bearer ${token}`,
                    },
                    body : JSON.stringify(content)
                }).then(res => res.json()).then(response =>{
                    this.fetchDataStaff();
                    alert(response.message);
                })
            }
            ; 
        }
        
        this.setState({
        validated : true,
        modalUpdate : false
        })
        
    }
    showRoleName(dataRole){
        if(dataRole === 1){
            return "Hệ thống";
        }else{
            if(dataRole === 2){
                return "Quản lý nhân viên";
            }else{
                if(dataRole === 3){
                    return "Quản lý bài đăng";
                }else{
                    if(dataRole === 4){
                        return "Chăm sóc khách hàng";
                    }else{
                        if(dataRole === 5){
                            return "Quản lý tài khoản";
                        }
                    }
                }
            }
        }
    }
    render(){
        return(
        <Table>
            <thead>
                 <tr>
                    {thArrStaff.map((prop,key)=> <th key={key}>{prop}</th>)}
                </tr>
            </thead>
            <tbody>
                 {this.state.data.map((prop,key) =>{
                     return(
                         <tr key={key}>
                             <td>{prop.id}</td>
                             <td>{prop.hoTen}</td>
                             <td>{prop.namSinh}</td>
                             <td>{prop.soDienThoai}</td>
                             <td>{prop.luong}</td>
                             <td>
                                 <Button onClick={() => this.openModalUpdate({prop})}>Cập nhật thông tin</Button>
                                 <Modal show={this.state.modalUpdate}
                                        onHide ={this.closeModalUpdate}
                                        size="lg"
                                        aria-labelledby="contained-modal-title-vcenter"
                                        centered
                                        >
                                        <Modal.Header closeButton>
                                            <Modal.Title id="contained-modal-title-vcenter">
                                                Cập nhật thông tin nhân viên 
                                            </Modal.Title>
                                        </Modal.Header>
                                        <Modal.Body>
                                        <Form className="container formAdd" noValidate validated={this.state.validated} onSubmit={(event)=>this.submitFormUpdate(event,this.state.dataUpdate.id)}>
                                            <Form.Row>
                                                <Form.Group as={Col} controlId="formGridName">
                                                    <Form.Label>Họ và Tên</Form.Label>
                                                    <Form.Control type="text" placeholder="Họ và tên" onChange={(event)=> this.isInputChange(event)} name="txtName" required
                                                        defaultValue={this.state.dataUpdate.hoTen}
                                                    />
                                                    <Form.Control.Feedback type="invalid">Vui lòng nhập họ và tên</Form.Control.Feedback>
                                                </Form.Group>
                                                <Form.Group as={Col} controlId="formGridUsername">
                                                    <Form.Label>Username</Form.Label>
                                                    <Form.Control type="text" placeholder="Username" onChange={(event)=> this.isInputChange(event)} name="txtUsername" required
                                                    defaultValue={this.state.dataUpdate.username}
                                                    />
                                                    <Form.Control.Feedback type="invalid">Vui lòng nhập username</Form.Control.Feedback>
                                                </Form.Group>
                                            </Form.Row> 
                                            <Form.Row>
                                                <Form.Group as={Col} controlId="formGridBirthDay">
                                                    <Form.Label>Năm sinh</Form.Label>
                                                    <Form.Control type="number" pattern="[1-2\s]{1}[0-9\s]{3}" min="1900" max="2020" maxLength="4" onChange={(event)=> this.isInputChange(event)}  name="txtBirthDay" required placeholder="Năm sinh"
                                                        defaultValue={this.state.dataUpdate.namSinh}
                                                    />
                                                    <Form.Control.Feedback type="invalid">Vui lòng năm sinh đúng định dạng VD:1998,2000,2001,...</Form.Control.Feedback>
                                                </Form.Group>
                                                <Form.Group as={Col} controlId="formGridPhoneNumber">
                                                    <Form.Label>Số điện thoại</Form.Label>
                                                    <Form.Control type="tel" pattern="0[0-9\s]{9}" maxLength="10" placeholder="Số điện thoại" onChange={(event)=> this.isInputChange(event)} name="txtPhoneNumber" required
                                                        defaultValue={this.state.dataUpdate.soDienThoai}
                                                    />
                                                    <Form.Control.Feedback type="invalid">Vui lòng số điện thoại đúng định dạng VD:0xxxxxxxxx</Form.Control.Feedback>
                                                </Form.Group>
                                            </Form.Row>
                                            <Form.Row>
                                                <Form.Group as={Col} controlId="formGridWage">
                                                    <Form.Label>Mức lương</Form.Label>
                                                    <InputGroup>
                                                        
                                                        <Form.Control type="text" onChange={(event)=> this.isInputChange(event)} placeholder="Mức lương" name="txtWage" required
                                                            defaultValue={this.state.dataUpdate.luong}
                                                        />
                                                        <InputGroup.Append>
                                                            <InputGroup.Text id="basic-addon2">VNĐ</InputGroup.Text>
                                                        </InputGroup.Append>
                                                        <Form.Control.Feedback type="invalid">Vui lòng nhập mức lương</Form.Control.Feedback>
                                                    </InputGroup>
                                                </Form.Group>
                                                <Form.Group as={Col} controlId="formGridRole">
                                                    <Form.Label>Chức vụ</Form.Label>
                                                    <Form.Control as="select" onChange={(event)=> this.isInputChange(event)} name="inputRole" required>
                                                        <option value={this.state.dataUpdate.roleType}>{this.showRoleName(this.state.dataUpdate.roleType)}</option>
                                                        {role.map((item,key)=>{
                                                            return(
                                                                <option key={key} value={item.id}>{item.tenquyen}</option>
                                                            );
                                                        })
                                                        }
                                                    </Form.Control>
                                                    <Form.Control.Feedback type="invalid">Vui lòng chọn chức vụ</Form.Control.Feedback>
                                                </Form.Group>
                                            </Form.Row>
                                            <div className="div-button-submit">
                                            <Button variant="primary" type="submit" className="submitFormAdd">
                                                Cập nhật
                                            </Button>
                                            </div>
                                        </Form>
                                        </Modal.Body>
                                    </Modal>
                             </td>
                         </tr>
                     );
                 })
                
                 }
            </tbody>
        </Table>  
        );
    }
}
export default TableStaff