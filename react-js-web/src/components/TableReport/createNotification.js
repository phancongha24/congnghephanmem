
import React from "react";
import {Form,Button} from "react-bootstrap";
import "../../assets/css/styles.css";

export default class CreateNotification extends React.Component{
    constructor(){
        super();
        this.state ={
            validated : false,
            txtTieuDe : null,
            txtNoiDung : null
        }
    }
    isInputChange = (event) =>{
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name] : value
        });
    }

    submitNotifi(event){
        const token = localStorage.token;
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
        }
        else{
            event.preventDefault();
            var content ={
                tieuDe : this.state.txtTieuDe,
                noiDung : this.state.txtNoiDung
            };
            if(token){
                fetch("http://localhost:8187/api/staffs/notifyall",{
                    method : "POST",
                    headers :{
                        "Content-Type" : "application/json",
                        'Authorization': `Bearer ${token}`,
                    },
                    body : JSON.stringify(content)
                }).then(res => res.json()).then(response =>{
                    alert(response.message);
                })
            }
        }
        this.setState({
            validated : true
        })
    }
    render(){
        return(
            <Form className="container formAdd" noValidate validated={this.state.validated} onSubmit={(event)=>this.submitNotifi(event)}>
                <Form.Group controlId="formBasicTitleNotifi">
                    <Form.Label>Tiêu đề thông báo</Form.Label>
                    <Form.Control type="text" placeholder="Nhập tiêu đề thông báo" onChange={(event)=> this.isInputChange(event)} name="txtTieuDe" required/>
                    <Form.Control.Feedback type="invalid">Vui lòng nhập tiêu đề</Form.Control.Feedback>
                </Form.Group>

                <Form.Group controlId="formBasicContentNotifi">
                    <Form.Label>Nội dung</Form.Label>
                    <Form.Control type="text" as="textarea" placeholder="Nhập nội dung" onChange={(event)=> this.isInputChange(event)} name="txtNoiDung" required/>
                    <Form.Control.Feedback type="invalid">Vui lòng nhập nội dung</Form.Control.Feedback>
                </Form.Group>
                <div className="div-button-submit">
                <Button variant="primary" type="submit" className="submitCreateNoti">
                    Tạo thông báo
                </Button>
                <Button variant="secondary" type="reset" className="buttonReset">
                    Xóa tất cả
                </Button>
                </div>
            </Form>
        );
    }
}