import React from "react";
import {Table} from "react-bootstrap";
import axios from "axios";
const thArrUser = ["ID người dùng","Họ tên","Số điện thoại","Email","Trạng thái"];


class ListAccount extends React.Component{
    constructor(){
        super();
        this.state = {
            data : [],
            trangthai: null
        }
    }
    fetchDataAccount(){
        const token = localStorage.token;
        if(token){
            axios.get("http://localhost:8187/api/users").then(response => {
                this.setState({
                    data : response.data
                })
            })
        }
    }
    componentDidMount(){
        this.fetchDataAccount();
    }
    render(){
        return (
        <Table>
            <thead>
            <tr>
                {thArrUser.map((prop,key)=> <th key={key}>{prop}</th>)}
            </tr>
            </thead>
            <tbody>
            {this.state.data.map((prop,key) => {
                return (
                    <tr key={key}>
                        <td>{prop.id}</td>
                        <td>{prop.hoTen}</td>
                        <td>{prop.soDienThoai}</td>
                        <td>{prop.email}</td>
                        <td>{!prop.locked ? "Hoạt động" : "Khóa"}</td>
                    </tr>
                );
            })}
            </tbody>
        </Table>
        );
    }
}
export default ListAccount;