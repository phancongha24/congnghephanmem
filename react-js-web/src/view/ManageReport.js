import React from "react";
import "../assets/css/styles.css";
import "react-bootstrap";
import Header from "../components/Header/Header.js";
import {Card,Nav} from "react-bootstrap";
import ListReport from "../components/TableReport/ListReport";
import ReportPost from "../components/TableReport/ReportPost";
import { BrowserRouter as Router,Switch, Route,NavLink} from "react-router-dom";
import CreateNotification from "../components/TableReport/createNotification";


function ManageReport() {
    return (
        <Router>
        <div className="container-fluid">
            <div className="row rowContent">
                <div className="mainContent col-lg-12">
                    <Header title="Báo cáo bài viết"/>
                    <Nav className="navContentTable" >
                                <Nav.Item className="navItem">
                                    <NavLink to="/listreport"
                                            className="navLink"
                                            activeStyle={{
                                                fontWeight: "bold",
                                                padding: "10px",
                                                backgroundColor: "#007bff",
                                                color: "#fff",
                                                borderRadius: "5px"
                                            }}
                                    >
                                        Danh sách báo cáo
                                    </NavLink>
                                </Nav.Item>
                                <Nav.Item className="navItem">
                                    <NavLink to="/reportpost" 
                                            exact
                                            className="navLink"
                                            activeStyle={{
                                            fontWeight: "bold",
                                            padding: "10px",
                                            backgroundColor: "#007bff",
                                            color: "#fff",
                                            borderRadius : "5px"
                                        }}
                                        >
                                            Xác nhận báo cáo
                                    </NavLink>
                                </Nav.Item>
                                <Nav.Item className="navItem">
                                    <NavLink to="/createNotifi" 
                                            exact
                                            className="navLink"
                                            activeStyle={{
                                            fontWeight: "bold",
                                            padding: "10px",
                                            backgroundColor: "#007bff",
                                            color: "#fff",
                                            borderRadius : "5px"
                                        }}
                                        >
                                            Tạo thông báo
                                    </NavLink>
                                </Nav.Item>
                        </Nav>
                </div>
            </div>
            <Switch>
                    <Route path="/listreport">
                        <Card className="cardContentTable">
                            <ListReport/>
                        </Card>
                    </Route>
                    <Route path="/reportpost">
                        <Card className="cardContentTable">
                            <ReportPost/>
                        </Card>
                    </Route>
                    <Route path="/createNotifi">
                        <Card className="cardContentTable">
                            <CreateNotification/>
                        </Card>
                    </Route>
                </Switch>
        </div>
        </Router>
);
}
export default ManageReport;