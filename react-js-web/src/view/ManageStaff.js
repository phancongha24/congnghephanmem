import React from "react";
import "../assets/css/styles.css";
import "react-bootstrap";
import Header from "../components/Header/Header.js";
import {Card,Nav} from "react-bootstrap";
import ListStaff from "../components/TableStaff/ListStaff";
import FormAddStaff from "../components/TableStaff/FormAddStaff";
import {BrowserRouter as Router,NavLink,Switch,Route} from "react-router-dom";
function ManageStaff() {
    return (
        <Router>
        <div className="container-fluid">
                <div className="row rowContent">
                    <div className="mainContent col-lg-12">
                        <Header title="Quản lý nhân viên"/>
                        <Nav className="navContentTable" >
                                <Nav.Item className="navItem">
                                    <NavLink to="/staff"
                                            className="navLink"
                                            activeStyle={{
                                                fontWeight: "bold",
                                                padding: "10px",
                                                backgroundColor: "#007bff",
                                                color: "#fff",
                                                borderRadius: "5px"
                                            }}
                                    >
                                        Danh sách nhân viên
                                    </NavLink>
                                </Nav.Item>
                                <Nav.Item className="navItem">
                                    <NavLink to="/addstaff" 
                                            exact
                                            className="navLink"
                                            activeStyle={{
                                            fontWeight: "bold",
                                            padding: "10px",
                                            backgroundColor: "#007bff",
                                            color: "#fff",
                                            borderRadius : "5px"
                                        }}
                                        >
                                        Thêm nhân viên
                                    </NavLink>
                                </Nav.Item>
                        </Nav>
                    </div>
                </div>
                <Switch>
                    <Route path="/staff">
                        <Card className="cardContentTable">
                            <ListStaff/>
                        </Card>
                    </Route>
                    <Route path="/addstaff">
                        <Card className="cardContentTable">
                            <FormAddStaff/>
                        </Card>
                    </Route>
                </Switch>
        </div>
        </Router>
    );          
}
export default ManageStaff;